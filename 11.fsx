let testInput = "ne,ne,ne"

let input = System.IO.File.ReadAllText "11.txt"

type Point = { X: int; Y: int }

module Point =
    let add a b = {X = a.X + b.X; Y = a.Y + b.Y}

let CoordinateMap =
    Map.ofList
        [ ("n",  { X = 0;  Y = -1 })
          ("ne", { X = 1;  Y = -1 })
          ("se", { X = 1;  Y = 0 })
          ("s",  { X = 0;  Y = 1 })
          ("sw", { X = -1; Y = 1 })
          ("nw", { X = -1; Y = 0 }) ]

let pathStringToMoveCoordinates (path: string) =
    path.Split ','
    |> List.ofArray
    |> List.map (fun x -> Map.find x CoordinateMap)

let reduceMoves coordinates =
    List.reduce Point.add coordinates

let scanMoves coordinates =
    List.scan Point.add {X=0;Y=0} coordinates

let pathStringToCoordinate = pathStringToMoveCoordinates >> reduceMoves

let countPath {X=x; Y=y} =
    max (abs x) (abs y)


pathStringToCoordinate input
|> countPath

pathStringToMoveCoordinates input
|> scanMoves
|> List.maxBy countPath