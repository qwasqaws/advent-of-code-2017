let testInput = [|0;2;7;0|]
let input = [|4;1;15;12;0;9;9;5;5;8;7;3;14;5;12;3|]

let getElementToRedistribute list =
    list
    |> Array.indexed
    |> Array.maxBy snd
    |> fst

let step startIndex (array: int[]) =
    let currentIndex = (startIndex) % array.Length
    let currentValue = array.[currentIndex]
    Array.set array currentIndex (currentValue + 1)
    array

let tryAddToSet value set =
    let newSet = Set.add value set
    if (Set.count newSet) = (Set.count set)
    then Result.Error set
    else Result.Ok newSet


let doIt input =
    let mutable iterations = Set.empty
    let mutable array = input

    iterations <- Set.add (Array.copy array) iterations

    let mutable isDone = true
    let mutable count = 0
    while isDone do
        let index = getElementToRedistribute input
        let blocks = Array.get input index
        Array.set array index 0

        for i in [1 .. blocks] do
            step (index + i) input |> ignore
            tryAddToSet (Array.copy array) iterations
            |> Result.mapError (fun newSet -> isDone <- false; newSet)
            |> Result.map (fun newSet -> (iterations <- newSet); newSet)
            |> ignore
        count <- count + 1
    count


doIt (Array.copy testInput)

doIt (Array.copy input)

doIt [|0;1;0|]

getElementToRedistribute input