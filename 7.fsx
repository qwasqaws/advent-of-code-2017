open System.Text.RegularExpressions
open System

let testInput = """pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)"""

let inputString = System.IO.File.ReadAllText("7.txt")

type StructuredLine =
    { Name    : string
      Weight  : int
      Children: string list }

let structureFromLine line =
    let name     = Regex.Match(line, "^(\w+) ")  .Groups.[1].Value
    let weight   = Regex.Match(line, "\((\d+)\)").Groups.[1].Value |> int
    let children =
        Regex.Match(line, "-> (.+$)").Groups.[1].Value
            .Split([|", "|], StringSplitOptions.RemoveEmptyEntries)
        |> List.ofArray
        |> List.map (fun x -> x.Trim ())
    { Name = name; Weight = weight; Children = children }

let parseInput (input: string) =
    let lines = input.Split '\n' |> List.ofArray
    List.map structureFromLine lines

let structured = parseInput (* testInput // *)inputString

let allChildrenNames =
    List.collect (fun x -> x.Children) structured


let allNodeNames = List.map (fun x -> x.Name) structured

let bottom =
    List.except allChildrenNames allNodeNames
    |> List.map (fun x -> List.find (fun y -> y.Name = x) structured)
    |> List.sortBy (fun x -> x.Weight)

let findNode name list =
    list
    |> List.find (fun x -> x.Name = name)

let findParent name list =
    list
    |> List.tryFind (fun n -> List.exists (fun c -> c = name) n.Children)

let findChildren node list =
    List.map (fun c -> findNode c list) node.Children

let rec findNodeWeight node list =
    let children = findChildren node list
    node.Weight + (List.sumBy (fun x -> findNodeWeight x list) children)

let isNodeBalanced node list =
    let children = findChildren node list
    let weights =
        children
        |> List.map (fun x -> findNodeWeight x list)
    if weights.IsEmpty
    then
        true
    else
        weights
        |> List.forall ((=) weights.Head)

//isNodeBalanced (findNode "tknk" structured) structured

let rec findLevel node list =
    if Option.isNone node then 0 else
    let n = Option.get node
    findLevel (findParent n.Name list) list + 1

let findUnbalancedNode list =
    list
    |> List.filter (not << (fun n -> isNodeBalanced n list))
    |> List.maxBy (fun n -> findLevel (Some n) list)


findLevel (Some (findNode "nbyij" structured)) structured

findUnbalancedNode structured
|> findChildren <| structured
|> List.map (fun c -> findNodeWeight c structured)
|> List.sortDescending
|> (fun l -> l.[1] - l.[0])
