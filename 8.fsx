let testInput = """b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10"""

let input = (System.IO.File.ReadAllText "8.txt").Trim ()

type Instruction =
    { Register         : string
      Operation        : (int -> int -> int)
      Value            : int
      PredicateRegister: string
      PredicateOp      : (int -> int -> bool)
      PredicateValue   : int }

let predicateOpMap: Map<string, (int -> int -> bool)> =
    Map [ ("<",  (<))
          (">" , (>))
          ("==", (=))
          ("!=", (<>))
          ("<=", (<=))
          (">=", (>=)) ]

let getPredicateOp inp = Map.find inp predicateOpMap

let parseLine line =
    let matched = System.Text.RegularExpressions.Regex.Match(line, "^(\w+) (inc|dec) (-?\d+) if (\w+) (.+) (-?\d+)$")
    { Register          = matched.Groups.      [1].Value
      Operation         = match matched.Groups.[2].Value with
                            | "inc" -> (+)
                            | "dec" -> (-)
                            | op -> failwithf "Bad operation: %s" op
      Value             = matched.Groups.      [3].Value |> int
      PredicateRegister = matched.Groups.      [4].Value
      PredicateOp       = matched.Groups.      [5].Value |> getPredicateOp
      PredicateValue    = matched.Groups.      [6].Value |> int }


let parseInput (input: string) =
    input.Split '\n' |> List.ofArray
    |> List.map (fun l -> parseLine (l.Trim()))

let buildCpu instructionList =
    instructionList
    |> List.map (fun i -> (i.Register, 0))
    |> Map.ofList

let testPredicate cpu instruction =
    let registerValue = Map.find instruction.PredicateRegister cpu
    instruction.PredicateOp registerValue instruction.PredicateValue

let processInstruction cpu instruction =
    match testPredicate cpu instruction with
    | false -> cpu
    | true  ->
        let registerValue = Map.find instruction.Register cpu
        let newValue = instruction.Operation registerValue instruction.Value
        Map.add instruction.Register newValue cpu

let instructions = parseInput input
let cpu = buildCpu instructions

let processAndMax (cpu, currentMax) instructions =
    let newCpuStep = processInstruction cpu instructions
    let newMax =
        Map.toList newCpuStep
        |> List.maxBy snd
        |> snd
    (newCpuStep, (max newMax currentMax))


// Part 1
List.fold processInstruction cpu instructions
|> Map.toList
|> List.maxBy snd


// Part 2
List.fold processAndMax (cpu, 0) instructions
|> snd

// Alternative part 2
let biggestRegister cpu =
    List.maxBy snd (Map.toList cpu)

List.scan processInstruction cpu instructions
|> List.map biggestRegister
|> List.maxBy snd
